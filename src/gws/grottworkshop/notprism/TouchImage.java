package gws.grottworkshop.notprism;


import android.content.Context;
import android.content.Intent;
import android.view.MotionEvent;
import android.widget.ImageView;

public class TouchImage extends ImageView{
	
	Class myclass;
	Intent intent;

	public TouchImage(Context context, Class ourclass) {
		super(context);
		myclass=ourclass;
	}
	
	public TouchImage(Context context) {
		super(context);
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		intent = new Intent(this.getContext(), myclass);
		this.getContext().startActivity(intent);
		return super.onTouchEvent(event);
	}
}