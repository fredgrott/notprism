package gws.grottworkshop.notprism;

import android.content.Context;
import android.content.Intent;
import android.view.MotionEvent;
import android.widget.TextView;

public class TextViewOne extends TextView{
	
	Intent intent;

	public TextViewOne(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		intent = new Intent(this.getContext(), TVOneActivity.class);
		this.getContext().startActivity(intent);
		return super.onTouchEvent(event);
	}

}
